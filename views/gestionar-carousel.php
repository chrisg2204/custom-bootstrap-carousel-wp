<?php
/**
 * Comment Management Screen
 *
 * @package WordPress
 * @subpackage Administration
 */

/** Load WordPress Bootstrap */

?>

<!DOCTYPE html>
<html lang="en">
   <head>
      <title>Gestionar Carousel</title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" type="text/css" href="/wp-content/plugins/custom-bootstrap-carousel-wp/assets/bootstrap/css/bootstrap.min.css" />
      <link rel="stylesheet" type="text/css" href="/wp-content/plugins/custom-bootstrap-carousel-wp/assets/bootstrap/css/bootstrap-theme.min.css" />
      <link rel="stylesheet" type="text/css" href="/wp-content/plugins/custom-bootstrap-carousel-wp/assets/bootstrap-table/bootstrap-table.min.css" />
      <link rel="stylesheet" type="text/css" href="/wp-content/plugins/custom-bootstrap-carousel-wp/assets/font-awesome/css/font-awesome.min.css" />
      <link rel="stylesheet" type="text/css" href="/wp-content/plugins/custom-bootstrap-carousel-wp/assets/toastr/toastr.min.css" />
   </head>
   <body style="background-color:#F1F1F1">
      <div class="container">
         <div class="row" style="margin-top:1%">
            <div class="col-md-12">
               <div class="panel panel-default">
                  <div class="panel-heading text-center">
                     <b class="panel-title">Gestionar Carousel</b>
                  </div>
                  <div class="panel-body">
                     <ul class="nav nav-pills">
                        <li class="active">
                           <a href="#agregar" data-toggle="tab">Nuevo</a>
                        </li>
                        <li>
                           <a href="#listar" data-toggle="tab">Listar</a>
                        </li>
                     </ul>
                     <div id="myTabContent" class="tab-content">
                        <div class="tab-pane fade active in" id="agregar">
                        	<form id="formSendCarousel" name="formSendCarousel" enctype="multipart/form-data">
                        		<fieldset>
                        			<legend>Ingresar Carousel </legend>
                        			<div class="col-md-6" style="margin-top:1%">
                        				<label class="control-label" for="titulo">Título</label> 
                        				<input id="titulo" name="titulo" type="text" placeholder="Título" class="form-control input-md" required="">
                        			</div>
                        			<div class="col-md-6" style="margin-top:1%">
                        				<label class="control-label" for="subTitulo">Subtitulo</label> 
                        				<input id="subTitulo" name="subTitulo" type="text" placeholder="Título" class="form-control input-md" required="">
                        			</div>
                        			<div class="col-md-6" style="margin-top:1%">
                        				<label class="control-label" for="banner">Banner</label>
                        				<input id="banner" name="banner" class="input-file" type="file">
                        			</div>
                                 <div class="col-md-6" style="margin-top:1%">
                                    <label class="control-label" for="urlRedirec">Url</label> 
                                    <input id="urlRedirec" name="urlRedirec" type="text" placeholder="Url" class="form-control input-md" required="">
                                 </div>
                        		</fieldset>
                        	</form>
                        	<div class="col-md-12" style="padding-top: 1%; padding-bottom: 1%">
                        		<div class="text-center">
                        			<button id="buttonAddCarousel" name="buttonAddCarousel" class="btn btn-primary btn-lg">Enviar <i class="fa fa-check fa-lg" aria-hidden="true"></i></button>
                        		</div>
                        	</div>
                        </div>
                        <div class="tab-pane fade" id="listar">

                        	<table 
                              id="tableCarousel"
                              name="tableCarousel"
                              data-show-refresh="true"
                              data-page-list="[5, 10, 20, 50, 100, 200]"
                              data-side-pagination="server"
                              data-pagination="true"
                              data-show-columns="true"
                              data-show-toggle="true"
                              data-search="true">
                           </table>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </body>
   <!-- Modal Ver -->
   <div class="modal fade" id="modalViewCarousel" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="modalCarouselAriaL" aria-hidden="true">
      <div class="modal-dialog">
         <div class="modal-content">
            <div class="modal-header">
               <button name="btnCancel" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
               <h4 class="modal-title text-center">Modal ver carousel</h4>
            </div>
            <div class="modal-body">
               <fieldset>
                  <div class="col-md-6" style="margin-top:1%">
                     <span class="label label-default">Título:</span>
                     <p class="lead" id="modalTitulo"></p>
                  </div>
                  <div class="col-md-6" style="margin-top:1%">
                     <span class="label label-default">Subtitulo:</span>
                     <p class="lead" id="modalSubtitulo"></p>
                  </div>
                  <div class="col-md-6" style="margin-top:1%">
                     <span class="label label-default">Banner:</span>
                     <p class="lead" id="modalBanner"></p>
                  </div>
                  <div class="col-md-6" style="margin-top:1%">
                     <span class="label label-default">Url:</span>
                     <p class="lead" id="modalUrl"></p>
                  </div>
               </fieldset>
            </div>
            <div class="modal-footer">
               <button name="btnSalir" data-dismiss="modal" class="btn btn-default btn-lg"><span class="fa fa-close fa-lg"></span> Salir</button>
            </div>
         </div>
      </div>
   </div>
   <!-- Modal Ver -->
   <!-- Modal Editar -->
   <div class="modal fade" id="modalEditCarousel" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="modalCarouselAriaL" aria-hidden="true">
      <div class="modal-dialog">
         <div class="modal-content">
            <div class="modal-header">
               <button name="btnCancel" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
               <h4 class="modal-title text-center">Modal editar carousel</h4>
            </div>
            <div class="modal-body">
               <form id="formEditCarousel" name="formEditCarousel" enctype="multipart/form-data">
                  <fieldset>
                     <div class="col-md-6" style="margin-top:1%">
                        <label class="control-label" for="modalEditTitulo">Título</label> 
                        <input id="modalEditTitulo" name="modalEditTitulo" type="text" placeholder="Título" class="form-control input-md" required="">
                     </div>
                     <div class="col-md-6" style="margin-top:1%">
                        <label class="control-label" for="modalEditSubtitulo">Subtitulo</label> 
                        <input id="modalEditSubtitulo" name="modalEditSubtitulo" type="text" placeholder="Título" class="form-control input-md" required="">
                     </div>
                     <div class="col-md-6" style="margin-top:1%">
                        <label class="control-label" for="modalViewBanner">Banner</label>
                        <p class="lead" id="modalViewBanner"></p>
                     </div>
                     <div class="col-md-6" style="margin-top:1%">
                        <label class="control-label" for="modalEditUrlRedirec">Url</label> 
                        <input id="modalEditUrlRedirec" name="modalEditUrlRedirec" type="text" placeholder="Título" class="form-control input-md" required="">
                     </div>
                     <div class="col-md-12" style="margin-top:1%">
                        <label class="control-label" for="modalEditBanner">Nuevo</label>
                        <input id="modalEditBanner" name="modalEditBanner" class="input-file" type="file">
                     </div>
                  </fieldset>
               </form>
            </div>
            <div class="modal-footer">
               <button id="buttonEditCarousel" name="buttonEditCarousel" class="btn btn-warning btn-lg">Editar <i class="fa fa-pencil fa-lg" aria-hidden="true"></i></button>
               <button name="btnSalir" data-dismiss="modal" class="btn btn-default btn-lg"><span class="fa fa-close fa-lg"></span> Salir</button>
            </div>
         </div>
      </div>
   </div>
   <!-- Modal Editar -->
   <script type="text/javascript" src="/wp-content/plugins/custom-bootstrap-carousel-wp/assets/jquery/jquery-3.2.1.min.js"></script>
   <script type="text/javascript" src="/wp-content/plugins/custom-bootstrap-carousel-wp/assets/bootstrap/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="/wp-content/plugins/custom-bootstrap-carousel-wp/assets/toastr/toastr.min.js"></script>
   <script type="text/javascript" src="/wp-content/plugins/custom-bootstrap-carousel-wp/assets/bootstrap-table/bootstrap-table.min.js"></script>
   <script type="text/javascript" src="/wp-content/plugins/custom-bootstrap-carousel-wp/assets/bootstrap-table/bootstrap-table-locale-all.min.js"></script>

   <script type="text/javascript" src="/wp-content/plugins/custom-bootstrap-carousel-wp/assets/app/gestionar-carousel.js"></script>
   <script type="text/javascript" src="/wp-content/plugins/custom-bootstrap-carousel-wp/assets/app/AddCarousel.js"></script>
   <script type="text/javascript" src="/wp-content/plugins/custom-bootstrap-carousel-wp/assets/app/ListCarousel.js"></script>

</html>