<?php

require_once(dirname( __FILE__ )."/../utils/Util.php");
require_once(dirname( __FILE__ )."/../connection/MysqlConnect.php");

class DataSourceCarousel {

	private $connectInstance;

	public function __construct() {
		$configJson = file_get_contents(dirname( __FILE__ )."/../../config/database.json");
		$jsonDecode = Util::extractToJson($configJson);

		$this->connectInstance = new MysqlConnect($jsonDecode["host"], $jsonDecode["username"], $jsonDecode["passwd"], $jsonDecode["database"]);
	}

	public function __destruct() {
		$this->connectInstance->close();
	}

	public function getLastId() {
		return $this->connectInstance->insert_id;
	}

	public function execQuery($sql, $types, $params) {
		if ($sql !== "") {
			if (count($params) !== 0) {
				if ($statement = $this->connectInstance->prepare($sql)) {
					$statement->bind_param($types, ...$params);
					return $statement->execute();
				} else {
					var_dump($this->connectInstance->error);
				}
			} else {
				print_r("Array is empty.");
			}
		} else {
			print_r("SQL is empty.");
		}
	}

	public function execSelectAndCountQuery($sql) {
		if ($sql !== "") {
			if ($statement = $this->connectInstance->prepare($sql)) {
				$statement->execute();
				$statementResult = $statement->get_result();
				$numRows = $statementResult->num_rows;
				if ($numRows !== 0) {
					while ($file = $statementResult->fetch_assoc()) {
                        $arr[] = $file;   
                    }
				} else {
					$arr = [];
				}
				$response = json_encode(array("total" => $this->getNumberRows(), "rows" => $arr));

				return $response;
			} else {
				var_dump($this->connectInstance->error);
			}
		} else {
			print_r("SQL is empty.");	
		}
	}

	public function execSelectQuery($sql) {
		if ($sql !== "") {
			if ($statement = $this->connectInstance->prepare($sql)) {
				$statement->execute();
				$statementResult = $statement->get_result();
				$numRows = $statementResult->num_rows;
				if ($numRows !== 0) {
					while ($file = $statementResult->fetch_assoc()) {
                        $arr[] = $file;   
                    }
				} else {
					$arr = [];
				}
				
				return $arr;
			} else {
				var_dump($this->connectInstance->error);
			}
		} else {
			print_r("SQL is empty.");	
		}
	}

	public function getNumberRows() {
		$sql="SELECT FOUND_ROWS()";
		$statement = $this->connectInstance->prepare($sql);
		$statement->execute();
		$statement->bind_result($num);
		$statement->fetch();
		return $num;
	}

}