<?php

Interface InterfaceCarousel {

	public function addCarousel(DataTransferCarousel $dtoCarousel);

	public function findAllCarousel(DataTransferCarousel $dtoCarousel);

	public function findOneCarousel(DataTransferCarousel $dtoCarousel);

	public function deleteCarousel(DataTransferCarousel $dtoCarousel);

	public function updateCarousel(DataTransferCarousel $dtoCarousel);
	
}