<?php

require_once(dirname( __FILE__ )."/../dataaccess/DataAccessCarousel.php");
require_once(dirname( __FILE__ )."/../datatransfer/DataTransferCarousel.php");
require_once(dirname( __FILE__)."/../utils/Util.php");

class BusinessCarousel {

	public function __construc() {}

	public function __destruct() {}

	public static function getTask($task) {
		$dtoCarousel = new DataTransferCarousel();
		$daoCarousel = new DataAccessCarousel();

		switch ($task) {
			case "CREATE":

			if (isset($_POST["titulo"]) && isset($_POST["subTitulo"]) && isset($_FILES["banner"])) {
				$dtoCarousel->setTitle($_POST["titulo"]);
				$dtoCarousel->setSubTitle($_POST["subTitulo"]);
				$dtoCarousel->setUrlRedirec($_POST["urlRedirec"]);
				$dtoCarousel->setFile($_FILES["banner"]);

				$daoCarousel->addCarousel($dtoCarousel);

			} else {
				print_r(Util::response(400, "Formulario incompleto.", true));
			}

				break;
			case "FIND_ALL":

			$searchType = $_GET["searchType"] ? $_GET["searchType"] : "all";
			
			$dtoCarousel->setSearchType($searchType);
			$dtoCarousel->setSearchValue($_GET["search"]);
			$dtoCarousel->setTitle($_GET["titulo"]);
			$dtoCarousel->setSubTitle($_GET["subTitulo"]);
			$dtoCarousel->setOrder($_GET["order"]);
			$dtoCarousel->setLimit($_GET["limit"]);
			$dtoCarousel->setOffset($_GET["offset"]);

			$daoCarousel->findAllCarousel($dtoCarousel);

				break;
			case "DELETE_ONE":

			if (isset($_GET["id"])) {
				$dtoCarousel->setId($_GET["id"]);

				$daoCarousel->deleteCarousel($dtoCarousel);

			} else {
				print_r(Util::response(400, "Formulario incompleto.", true));
			}

				break;
			case "UPDATE_ONE": 

			if ($_GET["id"]) {

				$dtoCarousel->setId($_GET["id"]);
				$dtoCarousel->setTitle($_POST["modalEditTitulo"]);
				$dtoCarousel->setSubTitle($_POST["modalEditSubtitulo"]);
				$dtoCarousel->setUrlRedirec($_POST["modalEditUrlRedirec"]);
				$dtoCarousel->setFile($_FILES["modalEditBanner"]);

				$daoCarousel->updateCarousel($dtoCarousel);

			} else {
				print_r(Util::response(400, "Formulario incompleto.", true));	
			}

				break;
			default:
			print_r(Util::response(404, "Task not found", true));
				break;
			}
	}
}
