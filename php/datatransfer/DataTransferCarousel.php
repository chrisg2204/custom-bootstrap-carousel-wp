<?php

class DataTransferCarousel {

	private $id;
	private $title;
	private $subTitle;
	private $imageName;
	private $urlPath;
	private $urlRedirec;
	private $file;
	private $searchValue;
	private $searchType;
	private $order;
	private $limit;
	private $offset;

	public function __construct() {}

	public function  __destruct() {}

	public function setId($id) {
		$value = strip_tags(stripslashes($id));
		$this->id = $value;
	}

	public function getId() {
		return $this->id;
	}

	public function setTitle($title) {
		$value = strip_tags(stripslashes($title));
		$this->title = $value;
	}

	public function getTitle() {
		return $this->title;
	}

	public function setSubTitle($subTitle) {
		$value = strip_tags(stripslashes($subTitle));
		$this->subTitle = $value;
	}

	public function getSubTitle() {
		return $this->subTitle;
	}

	public function setImageName($imageName) {
		$value = strip_tags(stripslashes($imageName));
		$this->imageName = $value;
	}

	public function getImageName() {
		return $this->imageName;
	}

	public function setUrlPath($urlPath) {
		$this->urlPath = $urlPath;
	}

	public function getUrlPath() {
		return $this->urlPath;
	}

	public function setUrlRedirec($urlRedirec) {
		$this->urlRedirec = $urlRedirec;
	}

	public function getUrlRedirec() {
		return $this->urlRedirec;
	}

	public function setFile($file) {
		$this->file = $file;
	}

	public function getFile() {
		return $this->file;
	}

	public function setSearchValue($searchValue) {
		$value = strip_tags(stripslashes($searchValue));
		$this->searchValue = $value;
	}

	public function getSearchValue() {
		return $this->searchValue;
	}

	public function setSearchType($searchType) {
		$value = strip_tags(stripslashes($searchType));
		$this->searchType = $value;
	}

	public function getSearchType() {
		return $this->searchType;
	}

	public function setOrder($order) {
		$value = strip_tags(stripslashes($order));
		$this->order = $value;
	}

	public function getOrder() {
		return $this->order;
	}

	public function setLimit($limit) {
		$value = strip_tags(stripslashes($limit));
		$this->limit = $value;
	}

	public function getLimit() {
		return $this->limit;
	}

	public function setOffset($offset) {
		$value = strip_tags(stripslashes($offset));
		$this->offset = $value;
	}

	public function getOffset() {
		return $this->offset;
	}
	
}