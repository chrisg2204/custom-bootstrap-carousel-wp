<?php

class MysqlConnect extends mysqli {

	public function __construct($host, $username, $passwd, $db) {
		parent::__construct($host, $username, $passwd, $db);

		if (mysqli_connect_error()) {
			die("Error de Conexión (". mysqli_connect_errno() . ")". mysqli_connect_error());
		}
	}
}