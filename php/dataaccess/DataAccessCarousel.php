<?php

require_once(dirname( __FILE__ ).'/../datasource/DataSourceCarousel.php');
require_once(dirname( __FILE__ ).'/../datatransfer/DataTransferCarousel.php');
require_once(dirname( __FILE__ ).'/../interface/InterfaceCarousel.php');
require_once(dirname( __FILE__ ).'/../utils/Util.php');

class DataAccessCarousel implements InterfaceCarousel {

	public function addCarousel(DataTransferCarousel $dtoCarousel) {
		$prepared = true;
		$response =  "";
		$dataSource = new DataSourceCarousel();

		$title = $dtoCarousel->getTitle();
		$subTitle = $dtoCarousel->getSubTitle();
		$urlRedirec = $dtoCarousel->getUrlRedirec();
		$file = $dtoCarousel->getFile();

		if ($title === "" || $title === null) {
			$response = Util::response(400, "Campo título requerido.", true);
			$prepared = false;
		}
		if ($subTitle === "" || $subTitle === null) {
			$response = Util::response(400, "Campo sub-titulo requerido.", true);
			$prepared = false;
		}
		if ($urlRedirec === "") {
			$response = Util::response(400, "Campo url requerido.", true);
			$prepared = false;
		}
		if ($file["size"] === 0 && $file["error"] === 4) {
			$response = Util::response(400, "No ha insertado banner aun.", true);
			$prepared = false;
		} else {
			$verifyMimetype = Util::verifyMimetype($file["name"]);
			if ($verifyMimetype["success"] === true) {
				if (!function_exists('wp_handle_upload')) {
					require_once(ABSPATH . 'wp-admin/includes/file.php');
				}

				$file['name'] = $verifyMimetype['newName'];
                $uploadedfile = $file;
                $upload_overrides = array('test_form' => false);
                $movefile = wp_handle_upload($uploadedfile, $upload_overrides);
			} else {
				$response = Util::response(400, "Formato de banner invalido.", true);
				$prepared = false;
			}
		}
		if ($prepared === true) {
			$getYear = date("Y");
			$getMonth = date("m");
			$urlPath = '/wp-content/uploads/'.$getYear.'/'.$getMonth.'/';

			$reqNewCarousel = array($title, $subTitle, $file["name"], $urlPath, $urlRedirec);
			$sqlNewCarousel = "INSERT INTO `casafly_wrdp1`.`wp_custom_carousel` (title, sub_title, image_name, url_path, url_redirec) VALUES (?, ?, ?, ?, ?)";

			$typesParamNewCarousel = "sssss";
			if ($dataSource->execQuery($sqlNewCarousel, $typesParamNewCarousel, $reqNewCarousel)) {
				$response = Util::response(200, "Carousel registrado con exito.", false);
			} else {
				$response = Util::response(500, "Error al registrar carousel.", true);
			}

			print_r($response);
		} else {
			print_r($response);
		}
	}

	public function findAllCarousel(DataTransferCarousel $dtoCarousel) {
		$condition = "";
		$dataSource = new DataSourceCarousel();

		$title = $dtoCarousel->getTitle();
		$subTitle = $dtoCarousel->getSubTitle();
		$searchType = $dtoCarousel->getSearchType();
		$order = $dtoCarousel->getOrder();
		$limit = $dtoCarousel->getLimit();
		$offset = $dtoCarousel->getOffset();


		if ($searchType === "all") {
			$searchValue = $dtoCarousel->getSearchValue();
			$condition .= "a.title LIKE '%$searchValue%' OR ";
			$condition .= "a.sub_title LIKE '%$searchValue%' OR ";
		} else if ($searchType === "particular") {
			if ($title !== "") {
				$condition .= "a.title LIKE '%$title%' OR ";
			}
			if ($subTitle !== "") {
				$condition .= "a.sub_title LIKE '%$subTitle%' OR ";
			}
		}

		if ($condition === "") {
			$condition = "1 = 1";
		} else {
			$condition = substr($condition, 0, -3);
		}

		$sqlFindCarousel = "SELECT
								SQL_CALC_FOUND_ROWS a.id AS 'idCarousel',
								a.title AS 'titulo',
								a.sub_title AS 'subTitulo',
								a.image_name AS 'nombreImagen',
								CONCAT(a.url_path, '', a.image_name) AS 'url',
								a.url_redirec as `urlRedirec`,
								a.created AS 'fechaCreacion'
							FROM `casafly_wrdp1`.`wp_custom_carousel` a WHERE $condition ORDER BY a.id $order LIMIT $limit OFFSET $offset";

		$execQuery = $dataSource->execSelectAndCountQuery($sqlFindCarousel);
		print_r($execQuery);
	}

	public function findOneCarousel(DataTransferCarousel $dtoCarousel) {
		$dataSource = new DataSourceCarousel();
		$id = $dtoCarousel->getId();
		$condition = "a.id = $id";

		$sqlFindOneCarousel = "SELECT
									a.id AS 'idCarousel',
									a.title AS 'titulo',a.sub_title AS 'subTitulo',
									a.image_name AS 'nombreImagen',
									a.url_path AS 'url',
									a.created AS 'fechaCreacion'
								FROM `casafly_wrdp1`.`wp_custom_carousel` a
									WHERE $condition LIMIT 1";

		$execQuery = $dataSource->execSelectQuery($sqlFindOneCarousel);
		return $execQuery;
	}

	// TODO Eliminar banner del server.
	public function deleteCarousel(DataTransferCarousel $dtoCarousel) {
		$prepared = true;
		$response =  "";
		$dataSource = new DataSourceCarousel();

		$id = $dtoCarousel->getId();

		if (!is_numeric($id)) {
			$response = Util::response(400, "Id no es un número.", true);
			$prepared = false;
		}
		if (count($this->findOneCarousel($dtoCarousel)) !== 1) {
			$response = Util::response(404, "Carousel no encontrado para eliminar.", true);
			$prepared = false;
		}

		if ($prepared === true) {
			$reqDeleteCarousel = array($id);
			$sqlDeleteCarousel = "DELETE FROM `casafly_wrdp1`.`wp_custom_carousel` WHERE id = ?";
			$typesParamDeleteCarousel = "i";

			if ($dataSource->execQuery($sqlDeleteCarousel, $typesParamDeleteCarousel, $reqDeleteCarousel)) {
				$response = Util::response(200, "Carousel eliminado con exito.", false);
			} else {
				$response = Util::response(500, "Error al eliminar carousel.", true);
			}

			print_r($response);
		} else {
			print_r($response);
		}
	}

	// TODO Eliminar banner del server.
	public function updateCarousel(DataTransferCarousel $dtoCarousel) {
		$prepared = true;
		$response =  "";
		$condition = "";
		$reqUpdateCarousel = array();
		$typesParamUpdateCarousel = "";
		$dataSource = new DataSourceCarousel();

		$id = $dtoCarousel->getId();
		$title = $dtoCarousel->getTitle();
		$subTitle = $dtoCarousel->getSubTitle();
		$urlRedirec = $dtoCarousel->getUrlRedirec();
		$file = $dtoCarousel->getFile();

		if (!is_numeric($id)) {
			$response = Util::response(400, "Id no es un número.", true);
			$prepared = false;
		}
		if ($title !== "" && $title !== null) {
			$condition .= "title = ?, ";
			array_push($reqUpdateCarousel, $title);
			$typesParamUpdateCarousel .= "s";
		}
		if ($subTitle !== "" && $subTitle !== null) {
			$condition .= "sub_title = ?, ";
			array_push($reqUpdateCarousel, $subTitle);
			$typesParamUpdateCarousel .= "s";
		}
		if ($urlRedirec !== "") {
			$condition .= "url_redirec = ?, ";
			array_push($reqUpdateCarousel, $urlRedirec);
			$typesParamUpdateCarousel .= "s";
		}
		if (count($this->findOneCarousel($dtoCarousel)) !== 1) {
			$response = Util::response(404, "Carousel no encontrado para actualizar.", true);
			$prepared = false;
		}
		if ($file !== null) {
			if ($file["size"] !== 0 && $file["error"] !== 4) {
				$verifyMimetype = Util::verifyMimetype($file["name"]);
				if ($verifyMimetype["success"] === true) {
					if (!function_exists('wp_handle_upload')) {
						require_once(ABSPATH . 'wp-admin/includes/file.php');
					}

					$file['name'] = $verifyMimetype['newName'];
					$uploadedfile = $file;
					$upload_overrides = array('test_form' => false);
					$movefile = wp_handle_upload($uploadedfile, $upload_overrides);
					$condition .= "image_name = ?, ";
					array_push($reqUpdateCarousel, $file['name']);
					$typesParamUpdateCarousel .= "s";
				} else {
					$response = Util::response(400, "Formato de banner invalido.", true);
					$prepared = false;
				}
			}
		}

		if ($prepared === true) {
			$condition = substr($condition, 0, -2);
			$sqlUpdateCarousel = "UPDATE `casafly_wrdp1`.`wp_custom_carousel` SET $condition WHERE id = $id";

			if ($dataSource->execQuery($sqlUpdateCarousel, $typesParamUpdateCarousel, $reqUpdateCarousel)) {
				$response = Util::response(200, "Carousel actualizado con exito.", false);
			} else {
				$response = Util::response(500, "Error al actualizar carousel.", true);
			}

			print_r($response);
		} else {
			print_r($response);
		}
	}

}