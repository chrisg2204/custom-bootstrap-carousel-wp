class ListCarousel {

	constructor() {
		this.task = '';
		this.pathAjax = '/wp-admin/admin-ajax.php?';

	}

	grid() {
		const self = this;
		self.task = 'FIND_ALL';

		$('#tableCarousel').bootstrapTable({
			method: 'GET',
			url: this.pathAjax + 'task=' + this.task + '&action=custom_bootstrap_carousel',
			locale: 'es-MX',
			columns: [{
				field: 'idCarousel',
				title: 'id',
				align: 'center'
			}, {
				field: 'titulo',
				title: 'Título',
				align: 'center'
			}, {
				field: 'subTitulo',
				title: 'Subtitulo',
				align: 'center'
			}, {
				field: 'fechaCreacion',
				title: 'Creado',
				align: 'center'
			}, {
				title: 'Acciones',
				align: 'center',
				formatter: this.formatterEvent,
				events: {
					'click .view': function(e, value, row, index) {
						self.viewCarousel(row);
					},
					'click .edit': function(e, value, row, index) {
						self.viewEditCarousel(e, row);
						self.editCarousel(e, row);
					},
					'click .remove': function(e, value, row, index) {
						self.deleteCarousel(e, row);
					}
				}
			}]
		});
	}

	formatterEvent(value, row, index) {
		let buttons = ['<a class="btn btn-default btn-xs view" href="javascript:void(0)" title="Ver">', '<i class="fa fa-check fa-lg"></i>', '</a>  ',
			'<a class="btn btn-warning btn-xs edit" href="javascript:void(0)" title="Editar">', '<i class="fa fa-pencil fa-lg"></i>', '</a>  ',
			'<a class="btn btn-danger btn-xs remove" href="javascript:void(0)" title="Eliminar">', '<i id=delete' + row.idCarousel + ' class="fa fa-remove fa-lg"></i>', '</a>'
		];

		return buttons.join('');
	}

	viewCarousel(row) {
		$('#modalTitulo').text(row.titulo);
		$('#modalSubtitulo').text(row.subTitulo);
		$('#modalBanner').empty();
		$('#modalBanner').append(`<a href="${row.url}" target="_blank">Click para ver Banner</a>`);
		$('#modalUrl').empty();
		$('#modalUrl').append(`<a href="http://${row.urlRedirec}" target="_blank">${row.urlRedirec}</a>`);

		this.toggleModal('#modalViewCarousel', 'show');
	}

	viewEditCarousel(e, row) {
		$('#modalEditTitulo').val(row.titulo);
		$('#modalEditSubtitulo').val(row.subTitulo);
		$('#modalEditUrlRedirec').val(row.urlRedirec);
		$('#modalViewBanner').empty();
		$('#modalViewBanner').append(`<a href="${row.url}" target="_blank">Click para ver Banner</a>`);

		this.toggleModal('#modalEditCarousel', 'show');
	}

	editCarousel(e, row) {
		const self = this;
		self.task = 'UPDATE_ONE';
		let parentEvent = e;

		$('#buttonEditCarousel').unbind('click').click((e) => {

			let formData = new FormData($('#formEditCarousel')[0]);

			$.ajax({
					url: this.pathAjax + 'task=' + this.task + '&id=' + row.idCarousel + '&action=custom_bootstrap_carousel',
					type: 'POST',
					data: formData,
					cache: false,
					contentType: false,
					processData: false,
					beforeSend: () => {
						$('#buttonEditCarousel').text(' Editando...').attr('disabled', 'disabled').prepend('<i class="fa fa-spinner fa-spin"></i>');
					}
				})
				.done(data => {
					console.log(data);
					let parseData = JSON.parse(data);
					if (parseData.status === 200) {
						toastr.success(parseData.data);
						self.cleanForm('#formEditCarousel');
					} else if (parseData.status === 400) {
						toastr.warning(parseData.data);
					} else if (parseData.status === 404) {
						toastr.warning(parseData.data);
					} else {
						toastr.error(parseData.data);
					}
				})
				.fail(err => {
					console.log(err);
					let parseErr = JSON.parse(err);
					toastr.error(parseErr.data);
				})
				.always(() => {
					$('#buttonEditCarousel').text(' Editar').removeAttr('disabled').prepend('<i class="fa fa-pencil fa-lg"></i>');
					$('#modalViewBanner').empty();
					self.toggleModal('#modalEditCarousel', 'hide');
					self.refreshGrid('#tableCarousel');

				});
			e.preventDefault();
		});
	}

	deleteCarousel(e, row) {
		const self = this;
		self.task = 'DELETE_ONE';

		let confirmr = confirm('Esta seguro de eliminar este Carousel ?');
		if (confirmr === true) {
			$.ajax({
					url: this.pathAjax + 'task=' + this.task + '&id=' + row.idCarousel + '&action=custom_bootstrap_carousel',
					type: 'GET',
					cache: false,
					beforeSend: () => {
						$('#delete' + row.idCarousel).removeClass('class="fa fa-remove fa-lg"').prepend('<i class="fa fa-spinner fa-spin"></i>');
					}
				})
				.done(data => {
					console.log(data);
					let parseData = JSON.parse(data);
					if (parseData.status === 200) {
						toastr.success(parseData.data);
					} else if (parseData.status === 400) {
						toastr.warning(parseData.data);
					} else if (parseData.status === 404) {
						toastr.warning(parseData.data);
					} else {
						toastr.error(parseData.data);
					}
				})
				.fail(err => {
					console.log(err);
					let parseErr = JSON.parse(err);
					toastr.error(parseErr.data);
				})
				.always(() => {
					self.refreshGrid('#tableCarousel');
				});

			e.preventDefault();

		} else {
			return false;
		}
	}

	refreshGrid(gridId) {
		$(gridId).bootstrapTable('refresh');
	}

	toggleModal(modalId, event) {
		$(modalId).modal(event);
	}

	cleanForm(formId) {
		$(formId)[0].reset();
	}

}