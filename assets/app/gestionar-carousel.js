$(document).ready(() => {
	const listCarousel = new ListCarousel();
	const addCarousel = new AddCarousel();

	listCarousel.grid();

	$('#buttonAddCarousel').click((e) => {
		addCarousel.add(e);
	});

});