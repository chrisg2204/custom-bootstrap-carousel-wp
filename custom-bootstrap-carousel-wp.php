<?php

/**
* Plugin Name: Custom bootstrap carousel
* Plugin URI: http://www.ejme.com.ve
* Description: Plugin para gestionar carousel de bootstrap.
* Version: 0.0.1
* Author: Edwin Mogollón <edwin.jme@hotmail.com>, Christian Giménez <christiang15@hotmail.com>
* Author URI: http://www.ejme.com.ve
**/

function gestionarCarousel() {
	require_once (dirname( __FILE__ )."/views/gestionar-carousel.php");
}

function customBootstrapCarousel() {
	add_menu_page("Custom Carousel", "Custom Carousel", "0", "gestionarCarousel", "", "dashicons-admin-customizer", 101);
	add_submenu_page('gestionarCarousel', 'Gestionar carousel', 'Gestionar carousel', '0', 'gestionarCarousel', 'gestionarCarousel');
}

	add_action("admin_menu", "customBootstrapCarousel");
	add_action('wp_ajax_custom_bootstrap_carousel','custom_bootstrap_carousel');
	add_action('wp_ajax_nopriv_custom_bootstrap_carousel','custom_bootstrap_carousel');

require_once(dirname( __FILE__ ).'/php/business/BusinessCarousel.php');

function custom_bootstrap_carousel() {
	if (isset($_GET["task"])) {
		BusinessCarousel::getTask($_GET["task"]);
	} else {
		print_r("Task is null or empty");
	}

	die();
}